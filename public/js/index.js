var nr = false;
function init(name) {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function(user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            var flag = false;
            firebase.database().ref('/user').once('value').then(function(snapshot) {
                snapshot.forEach(function(childshot) {
                    var childData = childshot.val();
                    if(childData.email === user_email){
                        flag = true;
                    }
                });
                if(flag === false){
                firebase.database().ref('/user').push({
                    email: user_email
                });
            }
            });
            
            menu.innerHTML = "<span class='dropdown-item'>" + user.email +  "<span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var lgobtn = document.getElementById("logout-btn");
            lgobtn.addEventListener('click', function(){
                firebase.auth().signOut().then(() => {
                    user_email = '';
                    alert("Logout Success");
                }).catch((error) => {
                    alert(error.message);
                });
            });

        } else {
            // It won't show any post if not login
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');

    post_btn.addEventListener('click', function() {
        if(name === ""){
        }
        else if (post_txt.value != "") {
            /// TODO 6: Push the post to database's "com_list" node
            ///         1. Get the reference of "com_list"
            ///         2. Push user email and post data
            ///         3. Clear text field

            firebase.database().ref('/com_list').push({
                email: user_email,
                towho: name,
                data:post_txt.value,
                type: 0,
                url:''
            });
            document.getElementById('comment').value = "";

        }
    });

    img_btn = document.getElementById('img_btn');

    img_btn.addEventListener('change', function(){
        /// TODO 2: Put the image to storage, and push the image to database's "com_list" node
        ///         1. Get the reference of firebase storage
        ///         2. Upload the image
        ///         3. Get the image file url, the reference of "com_list" and push user email and the image
        var file = img_btn.files[0];
        var path = "img/" + file.name;
        firebase.storage().ref(path).put(file).then(
            () =>{
                firebase.storage().ref(path).getDownloadURL().then((url) => {
            console.log(url);
        var data = {
            email: user_email,
            towho: name,
            // Type 1 for img
            type: 1,
            data: post_txt.value,
            url: url
        };
        firebase.database().ref('com_list').push(data);
        });
            }
        );
        

    });

    // The html code for post
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'><img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'><p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    var str_after_content = "</p></div></div>\n";

    var str_before_img = "<img class='img pt-2' style='height: 300px;' src='";
    var str_after_img = "'>";

    var postsRef = firebase.database().ref('/com_list');
    // List for store posts html
    var total_post = [];
    // Counter for checking history post update complete
    var first_count = 0;
    // Counter for checking when to update new post
    var second_count = 0;

    postsRef.once('value')
        .then(function(snapshot) {
            /// TODO 7: Get all history posts when the web page is loaded 
            ///         1. Get all history post and push to a list (str_before_username + email + </strong> + data + str_after_content)
            ///         2. count history message number and recond in "first_count"
            ///         Hint : Trace the code in this block, then you will know how to finish this TODO
            snapshot.forEach(function(childshot) {
                var childData = childshot.val();
                if(childData.email === user_email && childData.towho === name){
                    var pstr = childData.data;
                    var outputdata = "";
                    if(childData.type === 0){
                        if(pstr.includes("<") || pstr.includes(">") || pstr.includes("\"") || pstr.includes("\'")){
                            var i = 0;
                            for(i = 0; i < pstr.length; i++){
                                if(pstr[i] === "<"){
                                    outputdata = outputdata + "&lt;";
                                }
                                else if(pstr[i] === ">"){
                                    outputdata = outputdata + "&gt;";
                                }
                                else if(pstr[i] === "\""){
                                    outputdata = outputdata + "&quot;";
                                }
                                else if(pstr[i] === "\'"){
                                    outputdata = outputdata + "&apos;";
                                }
                                else{
                                    outputdata = outputdata + pstr[i];
                                }
                            }
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + outputdata + str_after_content;
                        }
                        else{
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                        }
                    }
                    else if(childData.type === 1){
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content;
                    }
                }
                else if(childData.email === name && childData.towho === user_email){
                    var pstr = childData.data;
                    var outputdata = "";
                    if(childData.type === 0){
                        if(pstr.includes("<") || pstr.includes(">") || pstr.includes("\"") || pstr.includes("\'")){
                            var i = 0;
                            for(i = 0; i < pstr.length; i++){
                                if(pstr[i] === "<"){
                                    outputdata = outputdata + "&lt;";
                                }
                                else if(pstr[i] === ">"){
                                    outputdata = outputdata + "&gt;";
                                }
                                else if(pstr[i] === "\""){
                                    outputdata = outputdata + "&quot;";
                                }
                                else if(pstr[i] === "\'"){
                                    outputdata = outputdata + "&apos;";
                                }
                                else{
                                    outputdata = outputdata + pstr[i];
                                }
                            }
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + outputdata + str_after_content;
                        }
                        else{
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                        }
                    }
                    else if(childData.type === 1){
                        total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content;
                    }
                }
                first_count += 1
            });
            /// Join all post in list to html in once
            document.getElementById('post_list').innerHTML = total_post.join('');

            /// Add listener to update new post
            postsRef.on('child_added', function(data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    if(childData.email === user_email && childData.towho === name){
                        var pstr = childData.data;
                        var outputdata = "";
                        if(childData.type === 0){
                            if(pstr.includes("<") || pstr.includes(">") || pstr.includes("\"") || pstr.includes("\'")){
                                var i = 0;
                                for(i = 0; i < pstr.length; i++){
                                    if(pstr[i] === "<"){
                                        outputdata = outputdata + "&lt;";
                                    }
                                    else if(pstr[i] === ">"){
                                        outputdata = outputdata + "&gt;";
                                    }
                                    else if(pstr[i] === "\""){
                                        outputdata = outputdata + "&quot;";
                                    }
                                    else if(pstr[i] === "\'"){
                                        outputdata = outputdata + "&apos;";
                                    }
                                    else{
                                        outputdata = outputdata + pstr[i];
                                    }
                                }
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + outputdata + str_after_content;
                            }
                            else{
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                            }
                        }
                        else if(childData.type === 1){
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content;
                        }
                    }
                    else if(childData.email === name && childData.towho === user_email){
                        var pstr = childData.data;
                        var outputdata = "";
                        if(childData.type === 0){
                            if(pstr.includes("<") || pstr.includes(">") || pstr.includes("\"") || pstr.includes("\'")){
                                var i = 0;
                                for(i = 0; i < pstr.length; i++){
                                    if(pstr[i] === "<"){
                                        outputdata = outputdata + "&lt;";
                                    }
                                    else if(pstr[i] === ">"){
                                        outputdata = outputdata + "&gt;";
                                    }
                                    else if(pstr[i] === "\""){
                                        outputdata = outputdata + "&quot;";
                                    }
                                    else if(pstr[i] === "\'"){
                                        outputdata = outputdata + "&apos;";
                                    }
                                    else{
                                        outputdata = outputdata + pstr[i];
                                    }
                                }
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + outputdata + str_after_content;
                            }
                            else{
                                total_post[total_post.length] = str_before_username + childData.email + "</strong>" + childData.data + str_after_content;
                            }
                        }
                        else if(dchildData.type === 1){
                            total_post[total_post.length] = str_before_username + childData.email + "</strong>" + str_before_img + childData.url + str_after_img + str_after_content;
                        }
                    }
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));
    
    var listsRef = firebase.database().ref('/user');
    var list_post = [];
    list_post[list_post.length] = "<a href='javascript:void(0)' class='closebtn' onclick='closeNav()'>&times;</a>";
    listsRef.once('value').then(function(snapshot){
        snapshot.forEach(function(childshot) {
            var childData = childshot.val();
            list_post[list_post.length] = "<a href='#' onclick='gotoPrivatRoom(\"" + childData.email + "\")'>" + childData.email + "</a>";
        });
        document.getElementById('mySidenav').innerHTML = list_post.join('');
    })
    .catch(e => console.log(e.message));

    //notififcation
    firebase.database().ref('/com_list').once('value').then(function(){nr = true;})
    firebase.database().ref('/com_list').on('child_added', function(data) {
        var childData = data.val();
        if (window.Notification && Notification.permission === "granted") {
            if(childData.towho === user_email && nr === true){
                var n = new Notification("Hi!  You have a new message");
            }
        }
    });
    var notifyConfig = {
        body: '\\ ^o^ /', // 設定內容
      };
      
      if (Notification.permission === 'default' || Notification.permission === 'undefined') {
        Notification.requestPermission(function(permission) {
          if (permission === 'granted') {
            // 使用者同意授權
            var notification = new Notification('Hi there!', notifyConfig); // 建立通知
            console.log(notification);
          }
        });
      }
}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  
function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

window.onload = function() {
    var name = "";
    init(name);
};

function gotoPrivatRoom(name){
    var Main = " <main role='main' class='container'>" + 
    "<div class='d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow'>" + 
        "<img class='mr-3' src='img/peko.jpg' alt='' width='48' height='48'>" + 
        "<div class='lh-100'>" +
            "<h6 class='mb-0 text-white lh-100'>" + name + "</h6>" +
        "</div>" + 
    "</div>" +
    "<div id='post_list'>" +
    "</div>" +
   " <div class='my-3 p-3 bg-white rounded box-shadow'>" +
        "<h5 class='border-bottom border-gray pb-2 mb-0'>New Post</h5>" +
        "<textarea class='form-control' rows='5' id='comment' style='resize:none'></textarea>" +
        "<div class='media text-muted pt-3'>" + 
            "<button id='post_btn' type='button' class='btn btn-success'>Submit</button>" +
            "<label class='pl-3' for='img_btn'>" +
            "<div class='btn btn-success' id='upload'>" +
            "<input type='file' id='img_btn' style='display:none;'>Upload Image" +
            "</div>" +
            "</label>" +
        "</div>" +
   "</div>" +
"</main>";

document.getElementById('mainfunc').innerHTML = Main;
init(name);
}

