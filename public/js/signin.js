function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(txtEmail.value, txtPassword.value).then(() => {
            create_alert("success", "");
        }).catch((error) => {
            create_alert("error", error.message);
            document.getElementById('inputEmail').value = "";
            document.getElementById('inputPassword').value = "";
        });
    });

    btnGoogle.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then((res) => {
            var token = res.credential.acessToken;
            var user = res.user;
            create_alert("success", "");
        }).catch((error) => {
            create_alert("error", error.message);
            document.getElementById('inputEmail').value = "";
            document.getElementById('inputPassword').value = "";
        });
    });

    btnSignUp.addEventListener('click', function() {
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value, txtPassword.value).then(() => {
            create_alert("success", "");
        }).catch((error) => {
            create_alert("error", error.message);
            document.getElementById('inputEmail').value = "";
            document.getElementById('inputPassword').value = "";
        });

    });
}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
        window.location.href = "index.html";
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function() {
    initApp();
};