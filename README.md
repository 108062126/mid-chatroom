# Software Studio 2021 Spring Midterm Project

## Notice

- Replace all [xxxx] to your answer

## Topic

- Project Name : mid chatroom

## Basic Components

|      Component       | Score | Y/N |
| :------------------: | :---: | :-: |
| Membership Mechanism |  15%  |  N  |
|    Firebase Page     |  5%   |  N  |
|       Database       |  15%  |  N  |
|         RWD          |  15%  |  N  |
|  Topic Key Function  |  20%  |  N  |

## Advanced Components

|      Component      | Score | Y/N |
| :-----------------: | :---: | :-: |
| Third-Party Sign In | 2.5%  |  N  |
| Chrome Notification |  5%   |  N  |
|  Use CSS Animation  | 2.5%  |  N  |
|   Security Report   |  5%   |  N  |

# 作品網址：https://mid-chatroom-2fd20.firebaseapp.com/

## Website Detail Description

1. CSS 動畫就是那個會一直旋轉的立體方塊
2. 登入後，點及左方的 chat list 可以和所有使用者進行 private roon 聊天

# Components Description :

# Other Functions Description :

1. Upload Image : 可以上傳圖片給對方
